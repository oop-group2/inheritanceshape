/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.inheritanceshape;

/**
 *
 * @author Natthakritta
 */
public class Rectangle extends Shape {

    private double wigth;
    private double height;

    public Rectangle(double wigth, double height) {
        this.wigth = wigth;
        this.height = height;
    }

    @Override
    public double calArea() {
        return (height * wigth);
    }

    @Override
    public void Show() {
        if (height == wigth) {
            System.out.println("Square = " + calArea());
        } else {
            System.out.println("Rectangle = " + calArea());
        }
    }
}
