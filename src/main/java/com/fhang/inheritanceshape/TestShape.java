/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.inheritanceshape;

/**
 *
 * @author Natthakritta
 */
public class TestShape {

    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.Show();

        Triangle triangle = new Triangle(3, 4);
        triangle.Show();
        
        Rectangle rectangle = new  Rectangle(3,4);
        rectangle.Show();
        
        Square square = new Square(2);
        square.Show();
        
        Circle circle2 = new Circle(4);
        circle2.Show();
        
        System.out.println();
        System.out.println("square is Shape: " + (square instanceof Shape));
        System.out.println("circle1 is Shape: " + (circle1 instanceof Shape));
        System.out.println("circle2 is Shape: " + (circle2 instanceof Shape));
        System.out.println("triangle is Shape: " + (triangle instanceof Shape));
        System.out.println("rectangle is Shape: " + (rectangle instanceof Shape));
        
        Shape[] shapeArea = {circle1,circle2,triangle,rectangle,square};
        System.out.println("--------Loop Show-------");
        for (int i = 0; i < shapeArea.length; i++) {
            shapeArea[i].Show();
        }
    }
}
