/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.inheritanceshape;

/**
 *
 * @author Natthakritta
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }
    @Override
    public double calArea(){
        return (height*base)/2;
    }
    
    @Override
    public void Show() {
        System.out.println("Triangle = "+calArea());
    }
}
